#ifndef __BITFLAG_MANGR__
#define __BITFLAG_MANGR__

#include "etypes.h"

#ifndef FLAGS_MAXFLAGS
	#define FLAGS_MAXFLAGS 256
#endif

extern u32 fm_flags[];

extern inline void set_flag(unv flag);
extern inline void inv_flag(unv flag);
extern inline void clr_flag(unv flag);
extern inline u8 get_flag(unv flag);

#endif
