#include <stdio.h>
#include <math.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_primitives.h>
#include "game.h"
#include "memman.h"
#include "stack.h"

#define PI 3.1415
#define TSIZE 64
#define ROOM_SIZE 4089
#define TPROOM ROOM_SIZE/TSIZE
#define VIEW_W 1024
#define VIEW_H 768
#define SPEED 10


ALLEGRO_DISPLAY *display = NULL;
ALLEGRO_EVENT_QUEUE *ev_queue = NULL;
ALLEGRO_TIMER *maintimer = NULL;
ALLEGRO_EVENT events;
u8 redraw = 0;
float delta = 0;
float gdelta = 0;

ALLEGRO_BITMAP* _tiles = 0x0;
ALLEGRO_BITMAP* _room = 0x0;

s16 posx = 0;
s16 posy = 0;
s8 kbud = 1;
s8 kblr = 1;

char InitializeAllegro(PROGRAMDATA* options) {
	if(!al_init()) {
		fprintf(stderr, "failed to initialize allegro!\n");
		return 0x0;
	}

	al_install_keyboard();
	al_init_primitives_addon();
	al_init_image_addon();

	al_set_new_display_flags(ALLEGRO_RESIZABLE);
	al_set_new_display_option(ALLEGRO_SAMPLE_BUFFERS, 1, ALLEGRO_SUGGEST);
	al_set_new_display_option(ALLEGRO_SAMPLES, 8, ALLEGRO_SUGGEST);
	al_set_new_display_option(ALLEGRO_RENDER_METHOD, 1, ALLEGRO_SUGGEST);
	al_set_new_display_option(ALLEGRO_CAN_DRAW_INTO_BITMAP, 1, ALLEGRO_REQUIRE);
	al_set_new_display_option(ALLEGRO_SUPPORT_NPOT_BITMAP, 1, ALLEGRO_REQUIRE);
	al_set_new_display_option(ALLEGRO_VSYNC, 1, ALLEGRO_SUGGEST);
	display = al_create_display(options->display_width, options->display_height);

	if(!display) {
		fprintf(stderr, "failed to create display!\n");
		return 0x0;
	}

	al_set_window_title(display, options->WindowTitle);

	maintimer = al_create_timer(1.0f/options->fps);
	al_start_timer(maintimer);

	ev_queue = al_create_event_queue();
	al_register_event_source(ev_queue, al_get_display_event_source(display));
	al_register_event_source(ev_queue, al_get_timer_event_source(maintimer));
	al_register_event_source(ev_queue, al_get_keyboard_event_source());
	
	if(!ev_queue || !maintimer)
	{
		fprintf(stderr, "Failed to initialize the timer/event queue!");
		return 0x0;
	}

	options->display = display;
	options->ev_queue = ev_queue;
	options->maintimer = maintimer;

	if(!st_init())
	{
		fprintf(stderr, "Failed to initialize the Memory Management module!");
		return 0x0;
	}
	
	return 0x1;
}

char LoadAssets() {
	u16 row = 0;
	u16 col = 0;
	u8 syoffset = 0;
	
	_tiles = al_load_bitmap("tiles.jpg");
	if(!_tiles)
		return 0x0;
	_room = al_create_bitmap(ROOM_SIZE,ROOM_SIZE);
	
	if(!_room)
		return 0x0;
	
	al_set_target_bitmap(_room);
	for(row = 0; row < TPROOM; row++)
	{
		for(col = 0; col < TPROOM; col++)
		{
			if(!(col%2))
				syoffset = (!(row%2))?64:0;
			else
				syoffset = ((row%2))?64:0;
			al_draw_bitmap_region(_tiles, 0, syoffset, TSIZE, TSIZE, col*TSIZE, row*TSIZE, 0L);
		}
	}
	
	return 0x1;
}

char doLogic() {
	posx -= (-1+kblr)*SPEED;
	posy -= (-1+kbud)*SPEED;
	if(posx > 0)
		posx = 0;
	if(posx < -ROOM_SIZE+VIEW_W)
		posx = -ROOM_SIZE+VIEW_W;
	if(posy > 0)
		posy = 0;
	if(posy < -ROOM_SIZE+VIEW_H)
		posy = -ROOM_SIZE+VIEW_H;
	return 1;
}

char DrawGUI() {
	al_draw_filled_rectangle(10,10,256,20,al_map_rgb(128,128,128));
	return 1;
}

char DrawScene() {
	//al_set_target_backbuffer(display);
	al_clear_to_color(al_map_rgb(0,0,0));
	al_draw_tinted_bitmap(_room, al_map_rgb_f(1,-posx*0.00035f,-posy*0.00035f), posx, posy, 0L);
	delta+=0.01f;
	gdelta = sinf(delta);
	return 1;
}

char ExitProgram(PROGRAMDATA* options)
{
	al_destroy_display(options->display);
	al_destroy_timer(options->maintimer);
	al_destroy_bitmap(_tiles);
	options->display = NULL;
	options->maintimer = NULL;
	_tiles = NULL;
	al_shutdown_image_addon();
	al_shutdown_primitives_addon();
	st_endall();
	return 0x0;
}

int main(int argc, char **argv){
	char wTitle[256];
	sprintf(wTitle, "Graphics");

	PROGRAMDATA prog;
	prog.display_width = 640;
	prog.display_height = 480;
	prog.fps = 30;
	prog.WindowTitle = wTitle;
	prog.ExitProgram = 0x0;

	if(!InitializeAllegro(&prog))
		return 0x1;

	if(!LoadAssets())
	{
		fprintf(stderr, "Failed to load bitmap!");
		ExitProgram(&prog);
		return 1;
	}

	while(!prog.ExitProgram)
	{
		al_wait_for_event(prog.ev_queue, &events);
		switch(events.type)
		{
			case ALLEGRO_EVENT_TIMER:
				redraw = 1;
			break;
			case ALLEGRO_EVENT_KEY_DOWN:
				switch(events.keyboard.keycode)
				{
					case ALLEGRO_KEY_ESCAPE:
						prog.ExitProgram = 0x1;
					break;
					case ALLEGRO_KEY_UP:
						kbud=0;
					break;
					case ALLEGRO_KEY_DOWN:
						kbud=2;
					break;
					case ALLEGRO_KEY_LEFT:
						kblr=0;
					break;
					case ALLEGRO_KEY_RIGHT:
						kblr=2;
					break;
				}
			break;
			case ALLEGRO_EVENT_KEY_UP:
				switch(events.keyboard.keycode)
				{
					case ALLEGRO_KEY_UP:
						kbud=1;
					break;
					case ALLEGRO_KEY_DOWN:
						kbud=1;
					break;
					case ALLEGRO_KEY_LEFT:
						kblr=1;
					break;
					case ALLEGRO_KEY_RIGHT:
						kblr=1;
					break;
				}
			break;
		}

		if(redraw)
		{
			al_set_target_backbuffer(prog.display);

			DrawScene();
			DrawGUI();

			al_flip_display();
		}else{
			/* Anything you want to do when not rendering */
		}
		
		doLogic();
	}

	ExitProgram(&prog);

	return 0;
}
