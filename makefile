CC = gcc
CFLAGS = -c -g -O2 -Wall
LDFLAGS = -lallegro -lallegro_image -lallegro_primitives -lm
TARGET = projectname
OBJECTS = $(patsubst %.c, %.o, $(wildcard *.c))

all: $(TARGET)

$(TARGET): $(OBJECTS)
	$(CC) -o $@ $^ $(LDFLAGS)

%.o: %.c
	$(CC) $(CFLAGS) $^ -o $@

clean:
	rm $(TARGET) $(OBJECTS)
