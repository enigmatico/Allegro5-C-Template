#include "bits.h"

u32 fm_flags[(FLAGS_MAXFLAGS >> 5)+1] = { 0x0L };

extern inline void set_flag(unv flag)
{
	fm_flags[flag>>5] |= (1L << flag%8);
}

extern inline void inv_flag(unv flag)
{
	fm_flags[flag>>5] ^= (1L << flag%8);
}

extern inline void clr_flag(unv flag)
{
	fm_flags[flag>>5] &= ~(1L << flag%8);
}

extern inline u8 get_flag(unv flag)
{
	return (fm_flags[flag>>5] >> (flag%8)) & 1L;
}
