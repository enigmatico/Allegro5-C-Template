#ifndef __GAME_H_COMMON__
#define __GAME_H_COMMON__

#include "etypes.h"
#include "bits.h"

typedef struct _programdata {
	ALLEGRO_DISPLAY* display;
	ALLEGRO_EVENT_QUEUE *ev_queue;
	ALLEGRO_TIMER *maintimer;
	unv display_width;
	unv display_height;
	unv fps;
	char* WindowTitle;
	s8 ExitProgram;
}(PROGRAMDATA);

#endif
